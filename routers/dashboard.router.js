const AdminBro = require("admin-bro");
const AdminBroExpress = require("admin-bro-expressjs");
const AdminBroMongoose = require("admin-bro-mongoose");
const theme = require("admin-bro-theme-dark");

const mongoose = require("mongoose");

AdminBro.registerAdapter(AdminBroMongoose);

const Orders = require("../models/orders.model");
const Steam = require("../models/steam.model");
const Bots = require("../models/bots.model");
const Settings = require("../models/settings.model");
const emails = require("../models/emails.model");

const adminBro = new AdminBro({
  databases: [mongoose],
  resources: [
    {
      resource: Orders,
      options: {
        parent: {
          name: "Side Panel",
          icon: "fa fa-cogs"
        },
        sort: {
          direction: "desc"
        }
      }
    },
    {
      resource: Steam,
      options: {
        parent: {
          name: "Side Panel",
          icon: "fa fa-cogs"
        },
        sort: {
          sortBy: "status",
          direction: "desc"
        }
      }
    },
    {
      resource: Bots,
      options: {
        parent: {
          name: "Side Panel",
          icon: "fa fa-cogs"
        }
      }
    },
    {
      resource: emails,
      options: {
        parent: {
          name: "Side Panel",
          icon: "fa fa-cogs"
        }
      }
    },
    {
      resource: Settings,
      options: {
        parent: {
          name: "Side Panel",
          icon: "fa fa-cogs"
        }
      }
    }
  ],

  rootPath: "/dashboard",
  logoutPath: "/dashboard/logout",
  loginPath: "/dashboard/login",
  basePath: "/",
  branding: {
    theme,
    companyName: "Steamify",
    softwareBrothers: false
  },
  dashboard: {},
  assets: {
    styles: [],
    scripts: [],
    globalsFromCDN: true
  }
});

const DASHBOARD = {
  email: process.env.ADMIN_EMAIL || "admin@admin.com",
  password: process.env.ADMIN_PASSWORD || "admin"
};

const router = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
  cookieName: process.env.ADMIN_COOKIE_NAME || "admin-bro",
  cookiePassword:
    process.env.ADMIN_COOKIE_PASS ||
    "supersecret-and-long-password-for-a-cookie-in-the-browser",
  authenticate: async (email, password) => {
    if (email === DASHBOARD.email && password === DASHBOARD.password) {
      return DASHBOARD;
    }
    return null;
  }
});

module.exports = router;

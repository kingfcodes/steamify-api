const express = require("express");
require("express-async-errors");
const path = require("path");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const people = require("./json/people.json");

const indexRouter = require("./routers/index.router");
const dashboardRouter = require("./routers/dashboard.router");
var cons = require("consolidate");
const MONGO_URL =
  process.env.MONGO_URL ||
  "mongodb://drcode:Cool123$@mongodb-5326-0.cloudclusters.net:10013/squad_db?authSource=admin";

const PORT = process.env.PORT || 80;

const app = express();

// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');

// view engine setup
app.engine("html", cons.swig);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "html");

app.use(express.static(path.join(__dirname, "public")));
app.use(bodyParser.json());

app.use("/dashboard", dashboardRouter);
app.use("/", indexRouter);
// app.use("/about", aboutRouter);
// app.use("/services", servicesRouter);
// app.use("/contact", contactRouter);
// app.use("/post-message", contactRouter.post);
// app.get("/profile", (req, res) => {
//   const person = people.profiles.find(p => p.id === req.query.id);
//   res.render("profile", {
//     title: `About ${person.firstname} ${person.lastname}`,
//     person
//   });
// });
const botsRoutes = require("./routers/api/bot.route");
const ordersRoutes = require("./routers/api/order.route");
const SteamRoutes = require("./routers/api/steam.route");

app.use("/api/bots", botsRoutes);
app.use("/api/orders", ordersRoutes);
app.use("/api/steam", SteamRoutes);
app.get("/api", (req, res) => {
  res.send("Api Route Running");
});

const run = async () => {
  await mongoose.connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  await app.listen(PORT, () => {
    console.log(`Example app listening on port ${PORT}!`);
  });
};

run();

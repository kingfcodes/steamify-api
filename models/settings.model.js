const { model } = require("mongoose");

const Settings = model("Settings", {
  subject: {
    type: String,
    required: true
  },
  Settings: {
    type: String,
    required: true
  }
});

module.exports = Settings;
